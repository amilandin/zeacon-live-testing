// replace these values with those generated in your TokBox Account
var apiKey = "46909054";
var sessionId =
  "1_MX40NjkwOTA1NH5-MTU5OTUxMTI0NTI4Mn41L2tNZDlmSGtOaFJ1SE5pai9oc3pNZU9-fg";
var token =
  "T1==cGFydG5lcl9pZD00NjkwOTA1NCZzaWc9MTA4ZDEzYTRjY2VkYTExOWYwMTc0MzAxNmZhNDM4YjkwZGU5MmJmMzpzZXNzaW9uX2lkPTFfTVg0ME5qa3dPVEExTkg1LU1UVTVPVFV4TVRJME5USTRNbjQxTDJ0TlpEbG1TR3RPYUZKMVNFNXBhaTlvYzNwTlpVOS1mZyZjcmVhdGVfdGltZT0xNTk5NTExMjcyJm5vbmNlPTAuMzgwOTcxMTg4NjE2MzA0NzUmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTYwMjEwMzI3MiZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==";

let myPublisher;

initializeSession();
document
  .getElementById("testing")
  .addEventListener("click", () => console.log("success"));

// Handling all of our errors here by alerting them
function handleError(error) {
  if (error) {
    alert(error.message);
  }
}

function initializeSession() {
  var session = OT.initSession(apiKey, sessionId);
  session.connect(token, async function (error) {
    if (error) {
      handleError(error);
    } else {
      myPublisher = OT.initPublisher(
        "publisher",
        {
          insertMode: "append",
          width: "100%",
          height: "100%",
        },
        function (error) {
          if (error) {
            handleError(error);
          } else {
            session.publish(myPublisher, handleError);
          }
        }
      );
    }
  });
}
